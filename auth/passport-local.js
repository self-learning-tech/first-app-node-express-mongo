var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var { userModel } = require('../models/userModel.js');

passport.use(new LocalStrategy(
  {
    usernameField: 'email_id',
    passwordField: 'password',
    session: false
  },
  function(email_id, password, done) {
    userModel.findOne({ email_id: email_id }, function (err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect email.' });
      }
      if (!user.verifyPassword(password)) {
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
));
