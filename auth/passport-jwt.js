var passport = require('passport');
var JwtStrategy = require('passport-jwt').Strategy;
var userModel = require('../models/userModel.js');
var jwtConfig = require('../config/jwt.js')

passport.use(new JwtStrategy(jwtConfig.jwtOptions, function(jwt_payload, done) {
    userModel.findOne({id: jwt_payload.user.id}, function(err, user) {
        if (err) {
            return done(err, false);
        }
        if (user) {
            return done(null, user);
        } else {
            return done(null, false);
        }
    });
}));
