const mongoose = require('mongoose');
const Schema   = mongoose.Schema;
const bcrypt = require('bcrypt');

const userSchema = new Schema({
	'name' : { type: String, required: true },
	'email_id' : { type: String, required: true },
	'password' : { type: String, required: true },
	'user_type' : { type: String, required: true },
	'created' : { type: Date, default: Date.now },
	'updated' : { type: Date, default: Date.now },
	'created_by' : { type: String, required: true },
	'updated_by' : { type: String, required: true }
});

userSchema.pre('save', function(next){
  const user = this;
  const hash = bcrypt.hashSync(this.password, 10);
  this.password = hash;
  next();
});

userSchema.methods.verifyPassword = function(password) {
		const user = this;
		const compare = bcrypt.compareSync(password, user.password);
		return compare;
};

module.exports = mongoose.model('user', userSchema);
