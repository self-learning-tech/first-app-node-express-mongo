const userModel = require('../models/userModel.js');

const isUserExist = function(email_id, password, callback) {
	userModel.findOne({ email_id: email_id }, function (err, user) {
		if (err) {
			callback({ message: 'Database error. Please try again.' });
		}
		if (!user) {
			callback({ message: 'Invalid user' });
		}
		if (!user.verifyPassword(password)) {
			callback({ message: 'Incorrect email or password' });
		} else {
			callback(null, user);
		}

	});
}

module.exports = {
	isUserExist
}
