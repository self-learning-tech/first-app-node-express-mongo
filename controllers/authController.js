const userModel = require('../models/userModel');
const userHelper = require('../models/userHelper');
const jwtHelper = require('../utils/jwt');

/**
 * authController.js
 *
 * @description :: Server-side logic for managing authentication.
 */
 
module.exports = {

    /**
     * authController.login()
     */
    login: function (req, res) {
      const requestData = req.body;
      if(requestData.email_id && requestData.password){
        const email_id = requestData.email_id;
        const password = requestData.password;
        const isUserExist = userHelper.isUserExist(email_id, password,
            function(err, user) {
              if (err){
                return res.status(401).json(err);
              } else {
                // generate jwt token
                const payload = { _id : user._id, email : user.email_id };
                const token = jwtHelper.createToken(payload);
                return res.json({ token });
              }
            }
        );
      }

    },
};
