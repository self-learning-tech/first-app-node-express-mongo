var ExtractJwt = require('passport-jwt').ExtractJwt;

var jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = 'S;4%*tm{EL';

module.exports = {
  jwtOptions
}
