const mongoose = require('mongoose');

const db = mongoose.connect('mongodb://localhost:27017/first-app').then(
  () => { console.info('MongoDB connected successfully.'); },
  err => { console.error('MongoDB connection failed.') }
);

module.exports = db;
