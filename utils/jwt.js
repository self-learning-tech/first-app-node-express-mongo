const jwtOptions = require('../config/jwt').jwtOptions
const jwt = require('jsonwebtoken');

const createToken = (payload) => {
  return jwt.sign({ user : payload }, jwtOptions.secretOrKey);
}
module.exports = {
  createToken
}
