var express = require('express');
var router = express.Router();
var authController = require('../controllers/authController.js');
var passport = require('passport');

/*
 * POST
 */
router.post('/login', authController.login);


module.exports = router;
