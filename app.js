var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

require('./config/db');

require('./auth/passport-jwt')

var passport = require('passport');
var bodyParser = require("body-parser");

var usersRouter = require('./routes/userRoutes');
var authRouter = require('./routes/authRoutes');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: false }));

app.use(passport.initialize());

app.use('/', authRouter);
app.use('/users', passport.authenticate('jwt', { session: false }), usersRouter);

module.exports = app;
